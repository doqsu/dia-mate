import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

class Btn extends Component {
    render(){
      return(
        <View style={styles.btn}>

          <TouchableOpacity
          onPress={this.deleteNote}>

              <Text style={styles.btnText}>
                  DELETE NOTE
              </Text>

          </TouchableOpacity>

        </View>
      )
    }
}

const styles = StyleSheet.create({
    btn: {
        width: "45%",
        marginTop: 10,
        marginBottom: 10,
        alignItems: 'center',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        padding: 5
      },
      btnText: {
        fontSize: 18,
      }
})


export default Btn;